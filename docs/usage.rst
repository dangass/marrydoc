################
[MarryDoc] Usage
################

.. contents::
    :local:

.. currentmodule:: marrydoc

*****************
Inherit DocString
*****************

The :func:`inherit` decorator copies a docstring from a specified object
and attaches it to the function or method it decorates. In the following
example, standard library :func:`~binascii.b2a_hex` function serves as
the docstring source:

:download:`inherit_example.py <inherit_example.py>`.

.. literalinclude:: inherit_example.py

The :func:`inherit` decorator guarentees that the docstrings are always
synchronized and does not require maintenance of the module when the original
docstring changes:

.. code-block:: python

    >>> import binascii
    >>> import inherit_example
    >>>
    >>> print binascii.hexlify.__doc__
    b2a_hex(data) -> s; Hexadecimal representation of binary data.

    This function is also available as "hexlify()".
    >>>
    >>> print inherit_example.hexlify.__doc__
    b2a_hex(data) -> s; Hexadecimal representation of binary data.

    This function is also available as "hexlify()".
    >>>
    >>> binascii.hexlify.__doc__ == inherit_example.hexlify.__doc__
    True


*************************
Maintain DocString Copies
*************************

The shortcoming of the :func:`inherit` decorator is that the copied docstring
does not appear in the module and makes module maintenance more difficult.
The :func:`copied_from` decorator defines a one to one relationship between
the source of the docstring and the docstring of the function it decorates:

    :download:`copied_from_example.py <copied_from_example.py>`.

    .. literalinclude:: copied_from_example.py


The defined relationship allows the |Command Line Tool| to check the
module's docstring:

.. code-block:: shell

    $ python -m marrydoc copied_from_example.py
    copied_from_example.py ... OK


If the docstring source changed, the command line tool updates the
docstring in the module when specifying the ``--merge`` option:

.. code-block:: shell

    $ python -m marrydoc --merge copied_from_example.py
    copied_from_example.py ... UPDATED


.. Note::
    During normal import of a module, the :func:`copied_from` decorator
    acts as a passthrough and introduces very little overhead.


*************************************
Maintain DocString With Modifications
*************************************

The :func:`based_on` decorator defines a relationship between the docstring
of the function it decorates and the basis from which it was derived.
The same as :func:`copied_from` and :func:`inherit`, the first argument
to :func:`based_on` is the program construct containing the docstring that
is to be tracked. :func:`based_on` requires a second argument that is a copy
of the source docstring (the source docstring is compared against the copy
and if they are unequal, the two values in combination with the actual
docstring facilitate a three way merge). For example:

    :download:`based_on_example.py <based_on_example.py>`.

    .. literalinclude:: based_on_example.py


The defined relationship to the source in combination with the docstring
copy provided as the second argument allow the |Command Line Tool| to
check if the source docstring has changed:

.. code-block:: shell

    $ python -m marrydoc based_on_example.py
    based_on_example.py ... OK


If the source docstring has changed, the |Command Line Tool| updates
the module's docstrings by performing a three way merge when specifying
the ``--merge`` option:

.. code-block:: shell

    $ python -m marrydoc --merge based_on_example.py
    based_on_example.py ... UPDATED


.. Note::
    The three way merge requires a merge tool of your choosing. Without
    configuring, the three way merge attempts usage of ``kdiff3``. See
    the next section for more information on configuring your favorite
    merge tool to be used.

    During normal import of a module, the :func:`based_on` decorator
    acts as a passthrough and introduces very little overhead.


**********************************
Configure Your Favorite Merge Tool
**********************************

For three way merges, the ``marrydoc`` command line tool uses ``kdiff3`` when
it is installed and in your system path. Otherwise ``marrydoc`` generates
"base", "left", and "right" files on your file system for you to merge manually.

To automatically invoke your favorite three way merge tool instead, set the
``MARRYDOC_MERGE`` environment variable and specify the command line invocation
using Python's string format substitution syntax.

For example, on Linux:

    .. code-block:: shell

        export MARRYDOC_MERGE="kdiff3 --merge --auto {base} {left} {right} --output {orig}"


Or on Microsoft Windows:

    .. code-block:: shell

        set MARRYDOC_MERGE="kdiff3 --merge --auto {base} {left} {right} --output {orig}"


.. Warning::
    The executable name/path must not contain spaces. The current implementation
    of ``marrydoc`` splits the string on whitespace and passes the result to a
    subprocess command.


**************************************
Test DocStrings In A Module Collection
**************************************

The :func:`~marrydoc.main` function exposes the command line interface
and offers a convenient method to check if the docstrings in a module
are up to date. Add a test case within the module's regression test to
call the command line interface and to check the returned exit code for
success indication (0).

The ``--walk`` option is useful for checking an entire package hierarchy,
for example:


    .. code-block:: python

        import os
        import unittest
        import marrydoc
        import mypackage

        class TestDocStrings(unittest.TestCase):

            def test_package(self):
                package_path = os.path.dirname(mypackage.__file__)
                exitcode = marrydoc.main(['--walk', package_path])
                self.assertEqual(exitcode, 0)


***************
Tips and Tricks
***************

- :func:`based_on`, :func:`copied_from` may also be used to decorate
  a class to wed its docstring to another. (:func:`inherit` cannot be
  used to decorate a class because the class docstring is not settable
  by the time the docorator executes.)

- The :func:`based_on`, :func:`copied_from`, and :func:`inherit`
  decorators may also be used in combination with the :func:`@classmethod`
  or :func:`staticmethod` decorators. The :mod:`marrydoc` decorator
  implementations accomodate decorating in either order. For better
  readability, place the :func:`@classmethod` and :func:`staticmethod`
  decorators first (on the outside).

- When decorating a method using :func:`based_on`, :func:`copied_from`,
  or :func:`inherit`, a class may be passed as the first argument
  to specify the source of the docstring. The docstring of the method by
  the same name in the specified class then acts as the docstring basis.

- Ensure your operating system path includes the ``Scripts`` subdirectory
  that is part of the normal Python installation. After installation, a
  ``marrydoc`` "shim" executable exists that subdirectory to invoke the
  command line tool directly:

    .. code-block:: shell

        $ marrydoc --help

- The ``marrydoc`` command line also accepts importable module and
  package names. Use this form when the module is in the Python
  system path. This form may be used in combination with the ``--walk``
  option to check an entire package, for example:

    .. code-block:: shell

        $ marrydoc baseline --walk
        /usr/local/lib/python3.5/dist-packages/baseline/__about__.py ... OK
        /usr/local/lib/python3.5/dist-packages/baseline/__init__.py ... OK
        /usr/local/lib/python3.5/dist-packages/baseline/__main__.py ... OK
        /usr/local/lib/python3.5/dist-packages/baseline/_baseline.py ... OK
        /usr/local/lib/python3.5/dist-packages/baseline/_script.py ... OK



.. |Command Line Tool| replace:: :ref:`command-line-tool`