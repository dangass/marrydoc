################
[baseline] About
################


************
Contributors
************

+ Dan Gass (dan.gass@gmail.com)
    - Primary author


***********
Development
***********

:Repository: https://gitlab.com/dangass/marrydoc


*******
License
*******

.. literalinclude:: ../LICENSE
