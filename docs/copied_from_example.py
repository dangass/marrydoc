import binascii
import marrydoc


@marrydoc.copied_from(binascii.b2a_hex)
def b2a_hex(data, sep=''):
    """b2a_hex(data) -> s; Hexadecimal representation of binary data.
    
    This function is also available as "hexlify()"."""
    mashed = binascii.b2a_hex(data)
    return sep.join(mashed[i:i+2] for i in range(0, len(mashed), 2))

hexlify = b2a_hex
