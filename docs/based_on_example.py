import binascii
import marrydoc


@marrydoc.based_on(
    binascii.b2a_hex,
    """b2a_hex(data) -> s; Hexadecimal representation of binary data.
    
    This function is also available as "hexlify()".""")
def b2a_hex(data, sep=''):
    """b2a_hex(data) -> s; Hexadecimal representation of binary data.
    b2a_hex(data, sep) -> s; Separated hexadecimal representation of binary data.

    This function is also available as "hexlify()"."""
    mashed = binascii.hexlify(data)
    return sep.join(mashed[i:i+2] for i in range(0, len(mashed), 2))

hexlify = b2a_hex
