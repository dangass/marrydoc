########################
[MarryDoc] Release Notes
########################

Versions are incremented according to `semver <http://semver.org/>`_.

***
0.2
***

+ 0.2.0 (2018-05-23)
    - Support specifying module name on command line.

***
0.1
***

+ 0.1.0 (2018-05-02)
    - Initial "beta" release.
