########################
[MarryDoc] API Reference
########################

.. contents::
    :local:

.. currentmodule:: marrydoc


**********
Decorators
**********

.. autofunction:: based_on

.. autofunction:: copied_from

.. autofunction:: inherit


.. _command-line-tool:

*****************
Command Line Tool
*****************

.. autofunction:: main

.. code-block:: shell

    $ python -m marrydoc --help
    usage: marrydoc [-h] [-m] [-v] [-w] path [path ...]

    Ensure related docstrings are consistent.

    positional arguments:
      path           module or directory path

    optional arguments:
      -h, --help     show this help message and exit
      -m, --merge    update module docstrings
      -w, --walk     recursively walk directories

.. Note::
    After installation, a ``marrydoc`` "shim" executable exists in the
    ``Scripts`` subdirectory of your Python installation. If your
    operating system has been configured to include the ``Scripts``
    subdirectory in the path, the tool may be invoked directly:

    .. code-block:: shell

        $ marrydoc --help
