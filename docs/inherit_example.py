import binascii
import marrydoc

@marrydoc.inherit(binascii.b2a_hex)
def b2a_hex(data, sep=''):
    mashed = binascii.b2a_hex(data)
    return sep.join(mashed[i:i+2] for i in range(0, len(mashed), 2))

hexlify = b2a_hex
